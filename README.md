# Installation

Clone via `git clone git@gitlab.com:nmalacarne/front_builder_env.git`

## Submodule Setup

```
git submodule init
git submodule update
```

## Vagrant Setup

```
vagrant plugin install vagrant-vbguest
vagrant up
vagrant ssh
```

## Rails Setup

```
cd front_builder
rails s -b 0.0.0.0 -p 8080
```

Visit `localhost:8080` to view the site.
