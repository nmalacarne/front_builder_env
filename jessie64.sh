#!/usr/bin/env bash

sudo apt-get update

sudo DEBIAN_FRONTEND=noninteractive apt-get install -y \
    autoconf \
    bison \
    build-essential \
    git \
    libssl-dev \
    libyaml-dev \
    libreadline6-dev \
    libmysqlclient-dev \
    libncurses5-dev \
    libffi-dev \
    libgdbm3 \
    libgdbm-dev \
    zlib1g-dev

if [ ! -d ~/.rbenv ]; then
    git clone https://github.com/rbenv/rbenv.git ~/.rbenv
fi

if [ ! -d ~/.rbenv/plugins/ruby-build ]; then
    git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
fi

if ! grep -q rbenv ~/.bash_profile; then
    echo -e '\nexport PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
    echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
fi

if ! grep -q document ~/.gemrc; then
    echo "gem: --no-document" > ~/.gemrc
fi

source .bash_profile

rbenv install 2.5.0
rbenv global 2.5.0

gem install bundler

(cd front_builder && bundle install && rails db:setup)
