class { 'rbenv': }

rbenv::plugin { 'rbenv/ruby-build': }

rbenv::build { '2.4.1':
  global => true
}

rbenv::gem { 'rails':
  ruby_version => '2.4.1'
}

class { '::mysql::server': }
